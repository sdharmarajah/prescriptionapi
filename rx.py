#!/usr/bin/env python
import time
from flask import Flask
from flask import request
from flask import jsonify
from pprint import pprint
from flask import make_response

from PrescriptionResponse import PatientData, DoctorData, ValidResponse, InvalidResponse

import json

import os

# Flask app should start in global layout
app = Flask(__name__)


@app.route('/prescription', methods=['POST'])
def main():
    if request.method == 'POST':

        data = request.data
        data_dict = json.loads(data)

        pprint("Received POST:")
        pprint(data_dict)

        password = data_dict.get('password')

        # check if password is correct
        if password == "test!":
            first_three = data_dict.get('first_three')
            reference_code = data_dict.get('reference_code')

            # create dummy patient and doctor
            # valid Rx, not expired = (doc_test1, patient_test1, currentTime - 3 days, Rx11111)
            # valid Rx, expired     = (doc_test2, patient_test2, currentTime - 10 days, Rx99999)
            # invalid Rx            = anything else

            doc_test1 = DoctorData(full_name="Dr. Janaka Wicremasinghe",
                                   speciality="General Physician",
                                   country="Sri Lanka",
                                   registrationNo="SLMC 19294",
                                   id_field="123")

            doc_test2 = DoctorData(full_name="Dr. Sabith Salieh",
                                   speciality="Family Physician",
                                   country="Sri Lanka",
                                   registrationNo="SLMC 34000",
                                   id_field="123")

            patient_test1 = PatientData(full_name="Heshan Fernando",
                                       gender="Male",
                                       age="31",
                                       id_field="669452591732")

            patient_test2 = PatientData(full_name="Heshan Fernando",
                                       gender="Male",
                                       age="31",
                                       id_field="669452591732")

            if first_three and reference_code:
                if reference_code == "16704" and first_three.lower() == "hes":
                    issue_date = int(time.time())*1000 - 3*24*60*60*1000
                    valid_response = ValidResponse(patient_data=patient_test1,
                                                   doctor_data=doc_test1,
                                                   issue_date=issue_date,
                                                   referenceCode=reference_code)
                    response = jsonify(valid_response.get_dict())
                    response.status_code = 200
                    return response

                elif reference_code == "21754" and first_three.lower() == "hes":
                    issue_date = int(time.time())*1000 - 10*24*60*60*1000
                    valid_response = ValidResponse(patient_data=patient_test2,
                                                   doctor_data=doc_test2,
                                                   issue_date=issue_date,
                                                   referenceCode=reference_code)
                    response = jsonify(valid_response.get_dict())
                    response.status_code = 200
                    return response

                else:
                    pprint("hi1")
                    invalid_response = InvalidResponse()
                    pprint("hi2")
                    return jsonify(invalid_response.get_dict())

            else:
                return "Invalid data sent. Doing nothing..."

        else:
            return "Wrong password!"


if __name__ == '__main__':
    port = int(os.getenv('PORT', 6000))

    print("Starting app on port %d" % port)

    app.run(debug=True, port=port, host='0.0.0.0')
