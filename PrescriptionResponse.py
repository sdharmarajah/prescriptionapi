import time
from flask import jsonify


class PatientData(object):

    def __init__(self, full_name, gender, age, id_field):

        # split name into first and last name (fix this later)
        self.first_name = full_name.split()[0]
        self.last_name = full_name.split()[1]
        self.gender = gender
        self.age = age
        self.id_field = id_field


class DoctorData(object):
    def __init__(self, full_name, speciality, country, registrationNo, id_field):
        self.full_name = full_name
        self.speciality = speciality
        self.country = country
        self.registrationNo = registrationNo
        self.id_field = id_field


class InvalidResponse(object):

    def __init__(self):
        pass

    def get_dict(self):
        dict = {"status": "invalid"}
        return dict


class ValidResponse(object):

    """Issue date is in ms"""
    def __init__(self, patient_data, doctor_data, issue_date, referenceCode):
        self.patient_data = patient_data
        self.doctor_data = doctor_data
        self.issue_date = issue_date
        self.referenceCode = referenceCode

        # calculate expired flag
        current_timestamp = int(time.time())*1000
        difference_in_s = (current_timestamp - issue_date)/1000
        if difference_in_s < 7*24*60*60:
            self.expired = False
        else:
            self.expired = True

    def get_dict(self):
        if self.expired:
            expiration_date = 'Valid til 4th July 2018'
            doctorSeal = 'http://sgp18.siteground.asia/~whistle4/test/prescriptionChecker/sabith_seal.png'
            doctorSignature = 'http://sgp18.siteground.asia/~whistle4/test/prescriptionChecker/sabith_signature.jpg'
        else:
            expiration_date = 'Valid til 18th July 2018'
            doctorSeal = 'http://sgp18.siteground.asia/~whistle4/test/prescriptionChecker/janaka_seal.png'
            doctorSignature = 'http://sgp18.siteground.asia/~whistle4/test/prescriptionChecker/janaka_signature.png'

        response = {
            'firstName': self.patient_data.first_name,
            'lastName': self.patient_data.last_name,
            'gender': self.patient_data.gender,
            'age': self.patient_data.age,
            'photoId': self.patient_data.id_field,
            'doctorName': self.doctor_data.full_name,
            'doctorCredentials': self.doctor_data.speciality + ' • Sri Lanka • ' + self.doctor_data.registrationNo,
            'doctorSeal': doctorSeal,
            'doctorSignature': doctorSignature,
            'status': 'Valid',
            'expired': self.expired,
            'expirationDate': expiration_date,
            'referenceCode': self.referenceCode
        }

        return response
