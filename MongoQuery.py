"""Return the prescription object from Mongo for a given referenceNo"""
import ssl
from pymongo import MongoClient


def getPrescription(referenceNo="#P-5491"):
    try:
        mongo_username = ""
        mongo_password = ""
        ssh_address = "18.204.63.245"
        private_key = "/Users/sohan/.ssh/odoc-life-gen2-live2-key.pem"
        # client = MongoClient("mongodb://" + mongo_username + ":" + mongo_password + "@" + ssh_address, ssl = True, ssl_keyfile = private_key)
        client = MongoClient("mongodb://" + ssh_address,
                             ssl=True,
                             ssl_keyfile=private_key)
        db = client.myDB

        #Should 'admin' be there or 'myDB'? 'admin' at least get if(auth) passed, while 'myDB' doesn't
        # auth = client.admin.authenticate(mongo_username,mongo_password)

        print("MongoDB connection successful")
        col = db.myCollection.count()
        print ("found col = " + str(col))

        client.close()
    except Exception as e:
        print("MongoDB connection failure: Please check the connection details")
        print(e)
